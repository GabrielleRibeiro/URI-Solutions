#include<bits/stdc++.h>

using namespace std;

int main(){
	int n, kg, kgTotal = 0;
	double value, total = 0;
	string fruits;

	cin >> n;

	for(int j = 1; j <= n; ++j){

		kg = 1;
		cin >> value;
		total += value;
		cin.ignore();
		getline(cin, fruits);
		
		for(int i = 0; fruits[i] != '\0'; ++i){
			if(fruits[i] == ' ')
				++kg;
		}
		
		kgTotal += kg;
		cout << "day " << j << ": " << kg << " kg\n";
	}
	
	printf("%.2lf kg by day\n", kgTotal * 1.0 / n);
	printf("R$ %.2lf by day\n", total / n);

	return 0;
}
