#include <bits/stdc++.h>

using namespace std;

int main(){
	int n, t, shots[55], value, shot;
	char jump[55];
	cin >> n;
	while(n--){
		value = 0;
		cin >> t;
		for(int i = 0; i < t; ++i){
			cin >> shot;
 			shots[i] = shot;
		}
		for(int i = 0; i < t; ++i){
			cin >> jump[i];
		}
		for(int i = 0; i < t; ++i){
			if(jump[i] == 'S' && shots[i] <= 2)
				value++;
			if(jump[i] == 'J' && shots[i] > 2)
				value++;
		}
		cout << value << "\n";
	}
	return 0;
}
