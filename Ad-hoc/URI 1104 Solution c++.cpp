#include <bits/stdc++.h>

using namespace std;

int main() {

	set<long long int> carda, cardb, card;
	long long int a, b, c, cardsize;
	
	cin >> a >> b;
	while(a != 0 && b != 0){
		carda.clear();
		cardb.clear();
		card.clear();

		while(a){
			cin >> c;
			carda.insert(c);
			card.insert(c);
			a--;
		}

		while(b){
			cin >> c;
			cardb.insert(c);
			card.insert(c);
			b--;
		
		}
		
		cardsize = carda.size() + cardb.size();

		if(carda.size() <= cardb.size()){
			cout << carda.size() - (cardsize - card.size()) << "\n";
		}
		else{
			cout << cardb.size() - (cardsize - card.size()) << "\n";
		}
		cin >> a >> b;
	}
	return 0;
}
