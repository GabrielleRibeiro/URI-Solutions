#include <bits/stdc++.h>

using namespace std;

int main() {

	long long int n, x, cont;
	vector <long long int> p;
	scanf("%lld", &n);
	while(n != 0){
		cont = 0;
		p.clear();
		while(n){
			scanf("%lld", &x);
                        p.push_back(x);
			n--;
		}
		
		for(long long int i = 0; i < p.size(); i++){
			
			if(i == 0){
				if(p[i+1] > p[i] && p[p.size()-1] > p[i] || p[i+1] < p[i] && p[p.size()-1] < p[i])
					cont++;
			}
			else if(i == p.size()-1){
				if(p[i-1] > p[i] && p[0] > p[i] || p[i-1] < p[i] && p[0] < p[i])
					cont++;
			}
			else{
				if((p[i+1] > p[i] && p[i-1] > p[i]) || (p[i+1] < p[i] && p[i-1] < p[i]))
					cont++;
			}
		}
		cout << cont << "\n";
		scanf("%lld", &n);
	}
	
	return 0;
}
