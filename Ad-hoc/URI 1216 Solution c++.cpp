#include <bits/stdc++.h>

using namespace std;

int main(){
	char name[100];
	int distance, totalDistances = 0, aux = 0;
	while(scanf(" %[^\n]s", name) != EOF){
		cin >> distance;
		totalDistances += distance;
		aux++;
	}
	printf("%.1lf\n", totalDistances * 1.0 / aux);
	return 0;
}
