#include <bits/stdc++.h>

using namespace std;

int main(){
	int l, r;
	while(scanf("%d %d", &l, &r), l != 0 && r != 0){
		cout << l + r << "\n";
	}
	return 0;
}
