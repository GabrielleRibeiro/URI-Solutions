# URI Solutions

## About This Repository
<p>This repository contains my solutions of the URI's problems. Most of then are in C++.</p>

## Attention!!!
<p>Be sure that you have tried enough before see the solutions here.  
Don't copy the solutions, see the code and try to undestand the logic behind the problem.
</p>

## What is URI?
<p>The URI Online Judge is a project being done by the Computer Science Department of the URI.
The main objective is to promote the practice of programming and knowledge sharing.</p>
<p> <a href="https://www.urionlinejudge.com.br/judge/pt/login"> URI Online Judge </a> </p>
<p>The problems of URI follow the model of Programming Contests questions.</p>

## What is a Programming Contest?
<p>A programming contest is a competition between teams, that try to solve the most problems in the shortest time.</p>

### C++
<p>C++ is the most used language in programming contests. 
This language has several standard libraries that implement data structures and algorithms.</p>

#### Header File bits/stdc++.h
<p>In the codes in C++ of this repository, we use the header file bits/stdc++.h which makes all necessary library additions.
This header file works if you are using the GCC compiler.</p>
