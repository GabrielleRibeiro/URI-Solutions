#include <bits/stdc++.h>

using namespace std;

int main(){
	string text;
	int n;
	bool aux;
	cin >> n;
	cin.ignore();
	while(n--){
		aux = true;
		getline(cin, text);
		for(int i = 0; text[i] != '\0'; ++i){
			if(text[i] >= 'a' && text[i] <= 'z' && aux){
				cout << text[i];
				aux = false;
			}
			if(text[i] == ' ')
				aux = true;		
		}
		cout << "\n";
	}
	return 0;
}
