#include <bits/stdc++.h>

using namespace std;

int main(){
	int n, a, nAux;
	while(scanf("%d", &n) != EOF){
		a = 1;
		nAux = n;
		while(a <= n){
			for(int i = 0; i < nAux/2; ++i){
				printf(" ");
			}
			for(int i = 0; i < a; ++i){
				printf("*");
			}
			printf("\n");
			a += 2;
			nAux -= 2;
		}
		a = 1;
		nAux = n;
		for(int i = 0; i < 2; ++i){
			for(int i = 0; i < nAux/2; ++i){
				printf(" ");
			}
			for(int i = 0; i < a; ++i){
				printf("*");
			}
			printf("\n");
			a += 2;
			nAux -= 2;
		}
		printf("\n");	
	}

	return 0;
}
