#include <bits/stdc++.h>

using namespace std;

int main(){
	int n, m;
	string a;
	cin >> n >> m;
	while(m--){
		cin >> a;
		if(a == "fechou")
			n++;
		else if(a == "clicou")
			n--;
	}
	cout << n << "\n";
	return 0;
}
