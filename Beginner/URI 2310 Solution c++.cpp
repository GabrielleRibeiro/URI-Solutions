#include <bits/stdc++.h>

using namespace std;

int main(){
	string name;
	double n, 
	       s, b, a, // Used to read values of services, blocks and attacks
	       s1 = 0, b1 = 0, a1 = 0, // Used to save values of service, block and attack attempts
	       s2 = 0, b2 = 0, a2 = 0; // Used to save values of service, block and attack that were succesfull
	cin >> n;
	while(n--){
		cin >> name;
		cin >> s >> b >> a;
		s1 += s;
		b1 += b;
		a1 += a;
		cin >> s >> b >> a;
		s2 += s;
		b2 += b;
		a2 += a;
	}
	printf("Pontos de Saque: %.2lf %%.\n", (100 * s2) / s1);  
	printf("Pontos de Bloqueio: %.2lf %%.\n", (100 * b2) / b1);
	printf("Pontos de Ataque: %.2lf %%.\n", (100 * a2) / a1);
	return 0;
}
