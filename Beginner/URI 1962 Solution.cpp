#include <bits/stdc++.h>

using namespace std;

int main(){

	long long int n, t;
	cin >> n;
	while(n--){
		cin >> t;
		if(t > 2015)
			cout << t - 2014 << " A.C.\n";
		else if(t < 2015)
			cout << 2015 - t << " D.C.\n";
		else
			cout << "1 A.C.\n";

	}

	return 0;
}
