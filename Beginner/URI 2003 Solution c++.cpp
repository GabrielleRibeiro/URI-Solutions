#include <bits/stdc++.h>

using namespace std;

int main(){
	int h, m, delay;
	while(scanf("%d:%d", &h, &m) != EOF){
		delay = 0;
		if((h+1) * 60 + m > 480){
			delay = ((h+1) * 60 + m) - 480;
		}
		cout << "Atraso maximo: " << delay << "\n";
	}
	return 0;
}
