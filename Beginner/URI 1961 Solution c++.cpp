#include <bits/stdc++.h>

using namespace std;

int main(){

	int p, n;
	cin >> p >> n;
	int q[n];
	for(int i = 0; i < n; ++i){
		cin >> q[i];
	}
	for(int i = 0; i < n - 1; ++i){
		if(q[i] - q[i + 1] > p || q[i + 1] - q[i] > p){
			cout << "GAME OVER\n";
			return 0;
		}
	}
	cout << "YOU WIN\n";
	return 0;
}
